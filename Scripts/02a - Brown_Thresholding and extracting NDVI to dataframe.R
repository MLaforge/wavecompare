########################################################################################################

######## Code for reading in NDVI files and prepping for pixel-by-pixel regression for ACENET ##########

######## Michel Laforge ################################################################################

######## Started March 7th 2017 ########################################################################

######## Modified December 2018 and February 2019 ######################################################

########################################################################################################

library(raster)
library(lme4)
library(car)
library(maptools)
library(plyr)
library(dplyr)
library(mosaic)
library(zoo)
library(minpack.lm)
library(sp)


##### 2007

### Read in the NDVI files

NDVI<-list.files('Input/NDVI/2007')
## read in lower baseline and upper ceiling rasters, as well as sample
## raster to properly set extent and resolution

winLow<-raster("Input/Thresholds/BrownLow.tif")
sumHigh<-raster("Input/Thresholds/BrownHigh.tif")
testRast<-raster("Input/NDVI/2007/FinalM2007257UTM.tif")

setwd('Input/NDVI/2007')
NDVIs<-sapply(NDVI, raster)
Jday<-as.numeric(substr(NDVI,11,13))## generate a vector of Julian Day

# Overlay 



## Crop and resample the baseline/ceiling rasters to the extent of the data

winLowrs<-raster::resample(winLow,testRast)
winLowCrop<-setExtent(winLowrs,ext=alignExtent(extent=testRast, object=winLowrs))

sumHighrs<-raster::resample(sumHigh,testRast)
sumHighCrop<-setExtent(sumHighrs,ext=alignExtent(extent=testRast, object=sumHighrs))

## Functions to be used for overlay. In winter, if the focal raster is less than
## winter baseline, replace with winter baseline. In summer, if focal raster is 
## greater than summer cieling, replace with summer ceiing.

myFunWin <- function(x, y) {ifelse(x < y, y, x)}
myFunSum <- function(x, y) {ifelse(x > y, y, x)}

##### test without lapply
#overTest<-overlay(testRast,winLowCrop, fun=myFunWin)
#overTest2<-overlay(overTest,sumHighCrop, fun=myFunSum)
##### End test without lapply

#### Doing the baselining, first for summer pixels, then for winter pixels

over1<-lapply(NDVIs, FUN=function(x)overlay(x,winLowCrop, fun=myFunWin))
over2<-lapply(over1, FUN=function(x)overlay(x,sumHighCrop, fun=myFunSum))

rm(over1)
rm(NDVIs)

## Rescale to between 0 and 1

#### Making a "Difference" layer for scaling purposes

Dif<-sumHighCrop-winLowCrop

## Example of scaling:
#(x-min)/dif

myFunScale<-function(focal,minim,dif) {(focal-minim)/dif}

gc()
## Single version:
#rescaleTest<-overlay(overTest2,winLowCrop,Dif,fun=myFunScale)

rescale<-lapply(over2, FUN=function(x)overlay(x,winLowCrop,Dif,fun=myFunScale))

## Replace all NA values with 1.1 (otherwise rasterToPoints skips NAs...1.1 allows for later subbing)
nareplace<-lapply(rescale, FUN=function(x) { x[is.na(x)] <- 1.1; x})


rm(over2)
rm(rescale)
gc()
## Raster to points, provides X, Y and the corresponding value as matrix 
extracted<-lapply(nareplace, FUN=function(x)rasterToPoints(x,na.rm=FALSE))  ### Doesn't keep NAs...

## Just take the values of the rasters as numeric
NDVIvec<-lapply(extracted, FUN=function(x)as.numeric(x[,3]))

## Put the values back into a table
rbinded <- t(do.call("rbind", NDVIvec))
rbinded[rbinded==1.1] <- 100
sums<-apply(rbinded, 1, FUN=function(x)sum(x))

##### FIND PROPER DATE THAT WORKS FOR ALL YEARS
ptsx<-extracted$FinalM2007049UTM.tif[,1]
ptsy<-extracted$FinalM2007049UTM.tif[,2]

Alldata<-cbind(ptsx,ptsy,rbinded,sums)

## Subset where at less than 15 of the 30 days are NA
(((ncol(Alldata)-3)/2)*100+100)
subbed<-subset(Alldata, sums<(((ncol(Alldata)-3)/2)*100+100))

MaskRastData<-data.frame(subbed[,1:3])
NewMask<-rasterFromXYZ(MaskRastData, crs = "+proj=utm +zone=21 +ellps=WGS84 +units=m +no_defs")

croppedRast<-lapply(nareplace,FUN=function(x)crop(x,NewMask))

MaskedRast<-lapply(croppedRast, FUN=function(x)mask(x,NewMask))

extracted2<-lapply(MaskedRast, FUN=function(x)rasterToPoints(x))  ### Doesn't keep NAs...

## Just take the values of the rasters as numeric
NDVIvec2<-lapply(extracted2, FUN=function(x)as.numeric(x[,3]))

## Put the values back into a table
rbinded2 <- t(do.call("rbind", NDVIvec2))

## replace NAs with the mean NDVI value for that day

#### Replace all NA values with the mean NDVI value for the entire year
rbinded2[rbinded2==1.1] <- NA

rbinded3<-apply(rbinded2,2,FUN=function(x)na.aggregate(x))

head(rbinded3)

rolled<-apply(rbinded3,1,FUN=function(x)rollmedian(x,k=3))
rolled2<-t(rolled)

rolled3<-rolled2+0.001

head(rolled3)

setwd("..")
setwd("..")
setwd("..")

saveRDS(rolled3, "Output/NDVISeries/BrownNDVIData2007.RDS")
saveRDS(extracted2, "Output/NDVISeries/BrownExtracted2007.RDS")

rm(list = ls())




##### 2008

### Read in the NDVI files

NDVI<-list.files('Input/NDVI/2008')
## read in lower baseline and upper ceiling rasters, as well as sample
## raster to properly set extent and resolution

winLow<-raster("Input/Thresholds/BrownLow.tif")
sumHigh<-raster("Input/Thresholds/BrownHigh.tif")
testRast<-raster("Input/NDVI/2008/FinalM2008257UTM.tif")

setwd('Input/NDVI/2008')
NDVIs<-sapply(NDVI, raster)
Jday<-as.numeric(substr(NDVI,11,13))## generate a vector of Julian Day

# Overlay 



## Crop and resample the baseline/ceiling rasters to the extent of the data

winLowrs<-raster::resample(winLow,testRast)
winLowCrop<-setExtent(winLowrs,ext=alignExtent(extent=testRast, object=winLowrs))

sumHighrs<-raster::resample(sumHigh,testRast)
sumHighCrop<-setExtent(sumHighrs,ext=alignExtent(extent=testRast, object=sumHighrs))

## Functions to be used for overlay. In winter, if the focal raster is less than
## winter baseline, replace with winter baseline. In summer, if focal raster is 
## greater than summer cieling, replace with summer ceiing.

myFunWin <- function(x, y) {ifelse(x < y, y, x)}
myFunSum <- function(x, y) {ifelse(x > y, y, x)}

##### test without lapply
#overTest<-overlay(testRast,winLowCrop, fun=myFunWin)
#overTest2<-overlay(overTest,sumHighCrop, fun=myFunSum)
##### End test without lapply

#### Doing the baselining, first for summer pixels, then for winter pixels

over1<-lapply(NDVIs, FUN=function(x)overlay(x,winLowCrop, fun=myFunWin))
over2<-lapply(over1, FUN=function(x)overlay(x,sumHighCrop, fun=myFunSum))

rm(over1)
rm(NDVIs)

## Rescale to between 0 and 1

#### Making a "Difference" layer for scaling purposes

Dif<-sumHighCrop-winLowCrop

## Example of scaling:
#(x-min)/dif

myFunScale<-function(focal,minim,dif) {(focal-minim)/dif}

gc()
## Single version:
#rescaleTest<-overlay(overTest2,winLowCrop,Dif,fun=myFunScale)

rescale<-lapply(over2, FUN=function(x)overlay(x,winLowCrop,Dif,fun=myFunScale))

## Replace all NA values with 1.1 (otherwise rasterToPoints skips NAs...1.1 allows for later subbing)
nareplace<-lapply(rescale, FUN=function(x) { x[is.na(x)] <- 1.1; x})


rm(over2)
rm(rescale)
gc()
## Raster to points, provides X, Y and the corresponding value as matrix 
extracted<-lapply(nareplace, FUN=function(x)rasterToPoints(x,na.rm=FALSE))  ### Doesn't keep NAs...

## Just take the values of the rasters as numeric
NDVIvec<-lapply(extracted, FUN=function(x)as.numeric(x[,3]))

## Put the values back into a table
rbinded <- t(do.call("rbind", NDVIvec))
rbinded[rbinded==1.1] <- 100
sums<-apply(rbinded, 1, FUN=function(x)sum(x))

##### FIND PROPER DATE THAT WORKS FOR ALL YEARS
ptsx<-extracted$FinalM2008049UTM.tif[,1]
ptsy<-extracted$FinalM2008049UTM.tif[,2]

Alldata<-cbind(ptsx,ptsy,rbinded,sums)

## Subset where at less than 15 of the 30 days are NA
(((ncol(Alldata)-3)/2)*100+100)
subbed<-subset(Alldata, sums<(((ncol(Alldata)-3)/2)*100+100))

MaskRastData<-data.frame(subbed[,1:3])
NewMask<-rasterFromXYZ(MaskRastData, crs = "+proj=utm +zone=21 +ellps=WGS84 +units=m +no_defs")

croppedRast<-lapply(nareplace,FUN=function(x)crop(x,NewMask))

MaskedRast<-lapply(croppedRast, FUN=function(x)mask(x,NewMask))

extracted2<-lapply(MaskedRast, FUN=function(x)rasterToPoints(x))  ### Doesn't keep NAs...

## Just take the values of the rasters as numeric
NDVIvec2<-lapply(extracted2, FUN=function(x)as.numeric(x[,3]))

## Put the values back into a table
rbinded2 <- t(do.call("rbind", NDVIvec2))

## replace NAs with the mean NDVI value for that day

#### Replace all NA values with the mean NDVI value for the entire year
rbinded2[rbinded2==1.1] <- NA

rbinded3<-apply(rbinded2,2,FUN=function(x)na.aggregate(x))

head(rbinded3)

rolled<-apply(rbinded3,1,FUN=function(x)rollmedian(x,k=3))
rolled2<-t(rolled)

rolled3<-rolled2+0.001

head(rolled3)

setwd("..")
setwd("..")
setwd("..")

saveRDS(rolled3, "Output/NDVISeries/BrownNDVIData2008.RDS")
saveRDS(extracted2, "Output/NDVISeries/BrownExtracted2008.RDS")

rm(list = ls())



##### 2009

### Read in the NDVI files

NDVI<-list.files('Input/NDVI/2009')
## read in lower baseline and upper ceiling rasters, as well as sample
## raster to properly set extent and resolution

winLow<-raster("Input/Thresholds/BrownLow.tif")
sumHigh<-raster("Input/Thresholds/BrownHigh.tif")
testRast<-raster("Input/NDVI/2009/FinalM2009257UTM.tif")

setwd('Input/NDVI/2009')
NDVIs<-sapply(NDVI, raster)
Jday<-as.numeric(substr(NDVI,11,13))## generate a vector of Julian Day

# Overlay 



## Crop and resample the baseline/ceiling rasters to the extent of the data

winLowrs<-raster::resample(winLow,testRast)
winLowCrop<-setExtent(winLowrs,ext=alignExtent(extent=testRast, object=winLowrs))

sumHighrs<-raster::resample(sumHigh,testRast)
sumHighCrop<-setExtent(sumHighrs,ext=alignExtent(extent=testRast, object=sumHighrs))

## Functions to be used for overlay. In winter, if the focal raster is less than
## winter baseline, replace with winter baseline. In summer, if focal raster is 
## greater than summer cieling, replace with summer ceiing.

myFunWin <- function(x, y) {ifelse(x < y, y, x)}
myFunSum <- function(x, y) {ifelse(x > y, y, x)}

##### test without lapply
#overTest<-overlay(testRast,winLowCrop, fun=myFunWin)
#overTest2<-overlay(overTest,sumHighCrop, fun=myFunSum)
##### End test without lapply

#### Doing the baselining, first for summer pixels, then for winter pixels

over1<-lapply(NDVIs, FUN=function(x)overlay(x,winLowCrop, fun=myFunWin))
over2<-lapply(over1, FUN=function(x)overlay(x,sumHighCrop, fun=myFunSum))

rm(over1)
rm(NDVIs)

## Rescale to between 0 and 1

#### Making a "Difference" layer for scaling purposes

Dif<-sumHighCrop-winLowCrop

## Example of scaling:
#(x-min)/dif

myFunScale<-function(focal,minim,dif) {(focal-minim)/dif}

gc()
## Single version:
#rescaleTest<-overlay(overTest2,winLowCrop,Dif,fun=myFunScale)

rescale<-lapply(over2, FUN=function(x)overlay(x,winLowCrop,Dif,fun=myFunScale))

## Replace all NA values with 1.1 (otherwise rasterToPoints skips NAs...1.1 allows for later subbing)
nareplace<-lapply(rescale, FUN=function(x) { x[is.na(x)] <- 1.1; x})


rm(over2)
rm(rescale)
gc()
## Raster to points, provides X, Y and the corresponding value as matrix 
extracted<-lapply(nareplace, FUN=function(x)rasterToPoints(x,na.rm=FALSE))  ### Doesn't keep NAs...

## Just take the values of the rasters as numeric
NDVIvec<-lapply(extracted, FUN=function(x)as.numeric(x[,3]))

## Put the values back into a table
rbinded <- t(do.call("rbind", NDVIvec))
rbinded[rbinded==1.1] <- 100
sums<-apply(rbinded, 1, FUN=function(x)sum(x))

##### FIND PROPER DATE THAT WORKS FOR ALL YEARS
ptsx<-extracted$FinalM2009049UTM.tif[,1]
ptsy<-extracted$FinalM2009049UTM.tif[,2]

Alldata<-cbind(ptsx,ptsy,rbinded,sums)

## Subset where at less than 15 of the 30 days are NA
(((ncol(Alldata)-3)/2)*100+100)
subbed<-subset(Alldata, sums<(((ncol(Alldata)-3)/2)*100+100))

MaskRastData<-data.frame(subbed[,1:3])
NewMask<-rasterFromXYZ(MaskRastData, crs = "+proj=utm +zone=21 +ellps=WGS84 +units=m +no_defs")

croppedRast<-lapply(nareplace,FUN=function(x)crop(x,NewMask))

MaskedRast<-lapply(croppedRast, FUN=function(x)mask(x,NewMask))

extracted2<-lapply(MaskedRast, FUN=function(x)rasterToPoints(x))  ### Doesn't keep NAs...

## Just take the values of the rasters as numeric
NDVIvec2<-lapply(extracted2, FUN=function(x)as.numeric(x[,3]))

## Put the values back into a table
rbinded2 <- t(do.call("rbind", NDVIvec2))

## replace NAs with the mean NDVI value for that day

#### Replace all NA values with the mean NDVI value for the entire year
rbinded2[rbinded2==1.1] <- NA

rbinded3<-apply(rbinded2,2,FUN=function(x)na.aggregate(x))

head(rbinded3)

rolled<-apply(rbinded3,1,FUN=function(x)rollmedian(x,k=3))
rolled2<-t(rolled)

rolled3<-rolled2+0.001

head(rolled3)

setwd("..")
setwd("..")
setwd("..")

saveRDS(rolled3, "Output/NDVISeries/BrownNDVIData2009.RDS")
saveRDS(extracted2, "Output/NDVISeries/BrownExtracted2009.RDS")

rm(list = ls())



##### 2010

### Read in the NDVI files

NDVI<-list.files('Input/NDVI/2010')
## read in lower baseline and upper ceiling rasters, as well as sample
## raster to properly set extent and resolution

winLow<-raster("Input/Thresholds/BrownLow.tif")
sumHigh<-raster("Input/Thresholds/BrownHigh.tif")
testRast<-raster("Input/NDVI/2010/FinalM2010257UTM.tif")

setwd('Input/NDVI/2010')
NDVIs<-sapply(NDVI, raster)
Jday<-as.numeric(substr(NDVI,11,13))## generate a vector of Julian Day

# Overlay 



## Crop and resample the baseline/ceiling rasters to the extent of the data

winLowrs<-raster::resample(winLow,testRast)
winLowCrop<-setExtent(winLowrs,ext=alignExtent(extent=testRast, object=winLowrs))

sumHighrs<-raster::resample(sumHigh,testRast)
sumHighCrop<-setExtent(sumHighrs,ext=alignExtent(extent=testRast, object=sumHighrs))

## Functions to be used for overlay. In winter, if the focal raster is less than
## winter baseline, replace with winter baseline. In summer, if focal raster is 
## greater than summer cieling, replace with summer ceiing.

myFunWin <- function(x, y) {ifelse(x < y, y, x)}
myFunSum <- function(x, y) {ifelse(x > y, y, x)}

##### test without lapply
#overTest<-overlay(testRast,winLowCrop, fun=myFunWin)
#overTest2<-overlay(overTest,sumHighCrop, fun=myFunSum)
##### End test without lapply

#### Doing the baselining, first for summer pixels, then for winter pixels

over1<-lapply(NDVIs, FUN=function(x)overlay(x,winLowCrop, fun=myFunWin))
over2<-lapply(over1, FUN=function(x)overlay(x,sumHighCrop, fun=myFunSum))

rm(over1)
rm(NDVIs)

## Rescale to between 0 and 1

#### Making a "Difference" layer for scaling purposes

Dif<-sumHighCrop-winLowCrop

## Example of scaling:
#(x-min)/dif

myFunScale<-function(focal,minim,dif) {(focal-minim)/dif}

gc()
## Single version:
#rescaleTest<-overlay(overTest2,winLowCrop,Dif,fun=myFunScale)

rescale<-lapply(over2, FUN=function(x)overlay(x,winLowCrop,Dif,fun=myFunScale))

## Replace all NA values with 1.1 (otherwise rasterToPoints skips NAs...1.1 allows for later subbing)
nareplace<-lapply(rescale, FUN=function(x) { x[is.na(x)] <- 1.1; x})


rm(over2)
rm(rescale)
gc()
## Raster to points, provides X, Y and the corresponding value as matrix 
extracted<-lapply(nareplace, FUN=function(x)rasterToPoints(x,na.rm=FALSE))  ### Doesn't keep NAs...

## Just take the values of the rasters as numeric
NDVIvec<-lapply(extracted, FUN=function(x)as.numeric(x[,3]))

## Put the values back into a table
rbinded <- t(do.call("rbind", NDVIvec))
rbinded[rbinded==1.1] <- 100
sums<-apply(rbinded, 1, FUN=function(x)sum(x))

##### FIND PROPER DATE THAT WORKS FOR ALL YEARS
ptsx<-extracted$FinalM2010049UTM.tif[,1]
ptsy<-extracted$FinalM2010049UTM.tif[,2]

Alldata<-cbind(ptsx,ptsy,rbinded,sums)

## Subset where at less than 15 of the 30 days are NA
(((ncol(Alldata)-3)/2)*100+100)
subbed<-subset(Alldata, sums<(((ncol(Alldata)-3)/2)*100+100))

MaskRastData<-data.frame(subbed[,1:3])
NewMask<-rasterFromXYZ(MaskRastData, crs = "+proj=utm +zone=21 +ellps=WGS84 +units=m +no_defs")

croppedRast<-lapply(nareplace,FUN=function(x)crop(x,NewMask))

MaskedRast<-lapply(croppedRast, FUN=function(x)mask(x,NewMask))

extracted2<-lapply(MaskedRast, FUN=function(x)rasterToPoints(x))  ### Doesn't keep NAs...

## Just take the values of the rasters as numeric
NDVIvec2<-lapply(extracted2, FUN=function(x)as.numeric(x[,3]))

## Put the values back into a table
rbinded2 <- t(do.call("rbind", NDVIvec2))

## replace NAs with the mean NDVI value for that day

#### Replace all NA values with the mean NDVI value for the entire year
rbinded2[rbinded2==1.1] <- NA

rbinded3<-apply(rbinded2,2,FUN=function(x)na.aggregate(x))

head(rbinded3)

rolled<-apply(rbinded3,1,FUN=function(x)rollmedian(x,k=3))
rolled2<-t(rolled)

rolled3<-rolled2+0.001

head(rolled3)

setwd("..")
setwd("..")
setwd("..")

saveRDS(rolled3, "Output/NDVISeries/BrownNDVIData2010.RDS")
saveRDS(extracted2, "Output/NDVISeries/BrownExtracted2010.RDS")

rm(list = ls())



##### 2011

### Read in the NDVI files

NDVI<-list.files('Input/NDVI/2011')
## read in lower baseline and upper ceiling rasters, as well as sample
## raster to properly set extent and resolution

winLow<-raster("Input/Thresholds/BrownLow.tif")
sumHigh<-raster("Input/Thresholds/BrownHigh.tif")
testRast<-raster("Input/NDVI/2011/FinalM2011257UTM.tif")

setwd('Input/NDVI/2011')
NDVIs<-sapply(NDVI, raster)
Jday<-as.numeric(substr(NDVI,11,13))## generate a vector of Julian Day

# Overlay 



## Crop and resample the baseline/ceiling rasters to the extent of the data

winLowrs<-raster::resample(winLow,testRast)
winLowCrop<-setExtent(winLowrs,ext=alignExtent(extent=testRast, object=winLowrs))

sumHighrs<-raster::resample(sumHigh,testRast)
sumHighCrop<-setExtent(sumHighrs,ext=alignExtent(extent=testRast, object=sumHighrs))

## Functions to be used for overlay. In winter, if the focal raster is less than
## winter baseline, replace with winter baseline. In summer, if focal raster is 
## greater than summer cieling, replace with summer ceiing.

myFunWin <- function(x, y) {ifelse(x < y, y, x)}
myFunSum <- function(x, y) {ifelse(x > y, y, x)}

##### test without lapply
#overTest<-overlay(testRast,winLowCrop, fun=myFunWin)
#overTest2<-overlay(overTest,sumHighCrop, fun=myFunSum)
##### End test without lapply

#### Doing the baselining, first for summer pixels, then for winter pixels

over1<-lapply(NDVIs, FUN=function(x)overlay(x,winLowCrop, fun=myFunWin))
over2<-lapply(over1, FUN=function(x)overlay(x,sumHighCrop, fun=myFunSum))

rm(over1)
rm(NDVIs)

## Rescale to between 0 and 1

#### Making a "Difference" layer for scaling purposes

Dif<-sumHighCrop-winLowCrop

## Example of scaling:
#(x-min)/dif

myFunScale<-function(focal,minim,dif) {(focal-minim)/dif}

gc()
## Single version:
#rescaleTest<-overlay(overTest2,winLowCrop,Dif,fun=myFunScale)

rescale<-lapply(over2, FUN=function(x)overlay(x,winLowCrop,Dif,fun=myFunScale))

## Replace all NA values with 1.1 (otherwise rasterToPoints skips NAs...1.1 allows for later subbing)
nareplace<-lapply(rescale, FUN=function(x) { x[is.na(x)] <- 1.1; x})


rm(over2)
rm(rescale)
gc()
## Raster to points, provides X, Y and the corresponding value as matrix 
extracted<-lapply(nareplace, FUN=function(x)rasterToPoints(x,na.rm=FALSE))  ### Doesn't keep NAs...

## Just take the values of the rasters as numeric
NDVIvec<-lapply(extracted, FUN=function(x)as.numeric(x[,3]))

## Put the values back into a table
rbinded <- t(do.call("rbind", NDVIvec))
rbinded[rbinded==1.1] <- 100
sums<-apply(rbinded, 1, FUN=function(x)sum(x))

##### FIND PROPER DATE THAT WORKS FOR ALL YEARS
ptsx<-extracted$FinalM2011049UTM.tif[,1]
ptsy<-extracted$FinalM2011049UTM.tif[,2]

Alldata<-cbind(ptsx,ptsy,rbinded,sums)

## Subset where at less than 15 of the 30 days are NA
(((ncol(Alldata)-3)/2)*100+100)
subbed<-subset(Alldata, sums<(((ncol(Alldata)-3)/2)*100+100))

MaskRastData<-data.frame(subbed[,1:3])
NewMask<-rasterFromXYZ(MaskRastData, crs = "+proj=utm +zone=21 +ellps=WGS84 +units=m +no_defs")

croppedRast<-lapply(nareplace,FUN=function(x)crop(x,NewMask))

MaskedRast<-lapply(croppedRast, FUN=function(x)mask(x,NewMask))

extracted2<-lapply(MaskedRast, FUN=function(x)rasterToPoints(x))  ### Doesn't keep NAs...

## Just take the values of the rasters as numeric
NDVIvec2<-lapply(extracted2, FUN=function(x)as.numeric(x[,3]))

## Put the values back into a table
rbinded2 <- t(do.call("rbind", NDVIvec2))

## replace NAs with the mean NDVI value for that day

#### Replace all NA values with the mean NDVI value for the entire year
rbinded2[rbinded2==1.1] <- NA

rbinded3<-apply(rbinded2,2,FUN=function(x)na.aggregate(x))

head(rbinded3)

rolled<-apply(rbinded3,1,FUN=function(x)rollmedian(x,k=3))
rolled2<-t(rolled)

rolled3<-rolled2+0.001

head(rolled3)

setwd("..")
setwd("..")
setwd("..")

saveRDS(rolled3, "Output/NDVISeries/BrownNDVIData2011.RDS")
saveRDS(extracted2, "Output/NDVISeries/BrownExtracted2011.RDS")

rm(list = ls())



##### 2012

### Read in the NDVI files

NDVI<-list.files('Input/NDVI/2012')
## read in lower baseline and upper ceiling rasters, as well as sample
## raster to properly set extent and resolution

winLow<-raster("Input/Thresholds/BrownLow.tif")
sumHigh<-raster("Input/Thresholds/BrownHigh.tif")
testRast<-raster("Input/NDVI/2012/FinalM2012257UTM.tif")

setwd('Input/NDVI/2012')
NDVIs<-sapply(NDVI, raster)
Jday<-as.numeric(substr(NDVI,11,13))## generate a vector of Julian Day

# Overlay 



## Crop and resample the baseline/ceiling rasters to the extent of the data

winLowrs<-raster::resample(winLow,testRast)
winLowCrop<-setExtent(winLowrs,ext=alignExtent(extent=testRast, object=winLowrs))

sumHighrs<-raster::resample(sumHigh,testRast)
sumHighCrop<-setExtent(sumHighrs,ext=alignExtent(extent=testRast, object=sumHighrs))

## Functions to be used for overlay. In winter, if the focal raster is less than
## winter baseline, replace with winter baseline. In summer, if focal raster is 
## greater than summer cieling, replace with summer ceiing.

myFunWin <- function(x, y) {ifelse(x < y, y, x)}
myFunSum <- function(x, y) {ifelse(x > y, y, x)}

##### test without lapply
#overTest<-overlay(testRast,winLowCrop, fun=myFunWin)
#overTest2<-overlay(overTest,sumHighCrop, fun=myFunSum)
##### End test without lapply

#### Doing the baselining, first for summer pixels, then for winter pixels

over1<-lapply(NDVIs, FUN=function(x)overlay(x,winLowCrop, fun=myFunWin))
over2<-lapply(over1, FUN=function(x)overlay(x,sumHighCrop, fun=myFunSum))

rm(over1)
rm(NDVIs)

## Rescale to between 0 and 1

#### Making a "Difference" layer for scaling purposes

Dif<-sumHighCrop-winLowCrop

## Example of scaling:
#(x-min)/dif

myFunScale<-function(focal,minim,dif) {(focal-minim)/dif}

gc()
## Single version:
#rescaleTest<-overlay(overTest2,winLowCrop,Dif,fun=myFunScale)

rescale<-lapply(over2, FUN=function(x)overlay(x,winLowCrop,Dif,fun=myFunScale))

## Replace all NA values with 1.1 (otherwise rasterToPoints skips NAs...1.1 allows for later subbing)
nareplace<-lapply(rescale, FUN=function(x) { x[is.na(x)] <- 1.1; x})


rm(over2)
rm(rescale)
gc()
## Raster to points, provides X, Y and the corresponding value as matrix 
extracted<-lapply(nareplace, FUN=function(x)rasterToPoints(x,na.rm=FALSE))  ### Doesn't keep NAs...

## Just take the values of the rasters as numeric
NDVIvec<-lapply(extracted, FUN=function(x)as.numeric(x[,3]))

## Put the values back into a table
rbinded <- t(do.call("rbind", NDVIvec))
rbinded[rbinded==1.1] <- 100
sums<-apply(rbinded, 1, FUN=function(x)sum(x))

##### FIND PROPER DATE THAT WORKS FOR ALL YEARS
ptsx<-extracted$FinalM2012049UTM.tif[,1]
ptsy<-extracted$FinalM2012049UTM.tif[,2]

Alldata<-cbind(ptsx,ptsy,rbinded,sums)

## Subset where at less than 15 of the 30 days are NA
(((ncol(Alldata)-3)/2)*100+100)
subbed<-subset(Alldata, sums<(((ncol(Alldata)-3)/2)*100+100))

MaskRastData<-data.frame(subbed[,1:3])
NewMask<-rasterFromXYZ(MaskRastData, crs = "+proj=utm +zone=21 +ellps=WGS84 +units=m +no_defs")

croppedRast<-lapply(nareplace,FUN=function(x)crop(x,NewMask))

MaskedRast<-lapply(croppedRast, FUN=function(x)mask(x,NewMask))

extracted2<-lapply(MaskedRast, FUN=function(x)rasterToPoints(x))  ### Doesn't keep NAs...

## Just take the values of the rasters as numeric
NDVIvec2<-lapply(extracted2, FUN=function(x)as.numeric(x[,3]))

## Put the values back into a table
rbinded2 <- t(do.call("rbind", NDVIvec2))

## replace NAs with the mean NDVI value for that day

#### Replace all NA values with the mean NDVI value for the entire year
rbinded2[rbinded2==1.1] <- NA

rbinded3<-apply(rbinded2,2,FUN=function(x)na.aggregate(x))

head(rbinded3)

rolled<-apply(rbinded3,1,FUN=function(x)rollmedian(x,k=3))
rolled2<-t(rolled)

rolled3<-rolled2+0.001

head(rolled3)

setwd("..")
setwd("..")
setwd("..")

saveRDS(rolled3, "Output/NDVISeries/BrownNDVIData2012.RDS")
saveRDS(extracted2, "Output/NDVISeries/BrownExtracted2012.RDS")

rm(list = ls())


##### 2013

### Read in the NDVI files

NDVI<-list.files('Input/NDVI/2013')
## read in lower baseline and upper ceiling rasters, as well as sample
## raster to properly set extent and resolution

winLow<-raster("Input/Thresholds/BrownLow.tif")
sumHigh<-raster("Input/Thresholds/BrownHigh.tif")
testRast<-raster("Input/NDVI/2013/FinalM2013257UTM.tif")

setwd('Input/NDVI/2013')
NDVIs<-sapply(NDVI, raster)
Jday<-as.numeric(substr(NDVI,11,13))## generate a vector of Julian Day

# Overlay 



## Crop and resample the baseline/ceiling rasters to the extent of the data

winLowrs<-raster::resample(winLow,testRast)
winLowCrop<-setExtent(winLowrs,ext=alignExtent(extent=testRast, object=winLowrs))

sumHighrs<-raster::resample(sumHigh,testRast)
sumHighCrop<-setExtent(sumHighrs,ext=alignExtent(extent=testRast, object=sumHighrs))

## Functions to be used for overlay. In winter, if the focal raster is less than
## winter baseline, replace with winter baseline. In summer, if focal raster is 
## greater than summer cieling, replace with summer ceiing.

myFunWin <- function(x, y) {ifelse(x < y, y, x)}
myFunSum <- function(x, y) {ifelse(x > y, y, x)}

##### test without lapply
#overTest<-overlay(testRast,winLowCrop, fun=myFunWin)
#overTest2<-overlay(overTest,sumHighCrop, fun=myFunSum)
##### End test without lapply

#### Doing the baselining, first for summer pixels, then for winter pixels

over1<-lapply(NDVIs, FUN=function(x)overlay(x,winLowCrop, fun=myFunWin))
over2<-lapply(over1, FUN=function(x)overlay(x,sumHighCrop, fun=myFunSum))

rm(over1)
rm(NDVIs)

## Rescale to between 0 and 1

#### Making a "Difference" layer for scaling purposes

Dif<-sumHighCrop-winLowCrop

## Example of scaling:
#(x-min)/dif

myFunScale<-function(focal,minim,dif) {(focal-minim)/dif}

gc()
## Single version:
#rescaleTest<-overlay(overTest2,winLowCrop,Dif,fun=myFunScale)

rescale<-lapply(over2, FUN=function(x)overlay(x,winLowCrop,Dif,fun=myFunScale))

## Replace all NA values with 1.1 (otherwise rasterToPoints skips NAs...1.1 allows for later subbing)
nareplace<-lapply(rescale, FUN=function(x) { x[is.na(x)] <- 1.1; x})


rm(over2)
rm(rescale)
gc()
## Raster to points, provides X, Y and the corresponding value as matrix 
extracted<-lapply(nareplace, FUN=function(x)rasterToPoints(x,na.rm=FALSE))  ### Doesn't keep NAs...

## Just take the values of the rasters as numeric
NDVIvec<-lapply(extracted, FUN=function(x)as.numeric(x[,3]))

## Put the values back into a table
rbinded <- t(do.call("rbind", NDVIvec))
rbinded[rbinded==1.1] <- 100
sums<-apply(rbinded, 1, FUN=function(x)sum(x))

##### FIND PROPER DATE THAT WORKS FOR ALL YEARS
ptsx<-extracted$FinalM2013049UTM.tif[,1]
ptsy<-extracted$FinalM2013049UTM.tif[,2]

Alldata<-cbind(ptsx,ptsy,rbinded,sums)

## Subset where at less than 15 of the 30 days are NA
(((ncol(Alldata)-3)/2)*100+100)
subbed<-subset(Alldata, sums<(((ncol(Alldata)-3)/2)*100+100))

MaskRastData<-data.frame(subbed[,1:3])
NewMask<-rasterFromXYZ(MaskRastData, crs = "+proj=utm +zone=21 +ellps=WGS84 +units=m +no_defs")

croppedRast<-lapply(nareplace,FUN=function(x)crop(x,NewMask))

MaskedRast<-lapply(croppedRast, FUN=function(x)mask(x,NewMask))

extracted2<-lapply(MaskedRast, FUN=function(x)rasterToPoints(x))  ### Doesn't keep NAs...

## Just take the values of the rasters as numeric
NDVIvec2<-lapply(extracted2, FUN=function(x)as.numeric(x[,3]))

## Put the values back into a table
rbinded2 <- t(do.call("rbind", NDVIvec2))

## replace NAs with the mean NDVI value for that day

#### Replace all NA values with the mean NDVI value for the entire year
rbinded2[rbinded2==1.1] <- NA

rbinded3<-apply(rbinded2,2,FUN=function(x)na.aggregate(x))

head(rbinded3)

rolled<-apply(rbinded3,1,FUN=function(x)rollmedian(x,k=3))
rolled2<-t(rolled)

rolled3<-rolled2+0.001

head(rolled3)

setwd("..")
setwd("..")
setwd("..")


saveRDS(rolled3, "Output/NDVISeries/BrownNDVIData2013.RDS")
saveRDS(extracted2, "Output/NDVISeries/BrownExtracted2013.RDS")

rm(list = ls())




