### Reformatting max and min NDVI values
### Michel Laforge
### Decmeber 19th/20th 2018

### Script to reformat rasters for baseline winter NDVI and summer maximum
### maximum NDVI based on Bischof et al's analysis. Product uses data from
### 2007 to 2013 and gets rid of all pixels coded as snow/ice, then finds
### the 0.03 quantile and sets it as a winter value, and finds the 0.93
### quantile to use as the max value.

### Taking NDVI quartile data produced by EE by Alec,
### reading in both bands, reprojecting into UTM and writing
### rasters for each band.


library(raster)
library(rgdal)

GreenLow<-raster('Input/Thresholds/NDVI.tif', band = 1)
GreenHigh<-raster('Input/Thresholds/NDVI.tif', band = 2)
CompLow<-raster('Input/Thresholds/NDVI.tif', band = 3)
CompHigh<-raster('Input/Thresholds/NDVI.tif', band = 4)
BrownLow<-raster('Input/Thresholds/NDVI.tif', band = 5)
BrownHigh<-raster('Input/Thresholds/NDVI.tif', band = 6)

plot(GreenLow)
plot(GreenHigh)
plot(CompLow)
plot(CompHigh)
plot(BrownLow)
plot(BrownHigh)

raster::projection(GreenLow)

proj <- "+proj=utm +zone=21 ellps=WGS84"

WinterLower<-projectRaster(ndvi, crs = proj)
SummerHigh<-projectRaster(upper, crs = proj)

GreenLow2<-projectRaster(GreenLow, crs = proj)
GreenHigh2<-projectRaster(GreenHigh, crs = proj)
CompLow2<-projectRaster(CompLow, crs = proj)
CompHigh2<-projectRaster(CompHigh, crs = proj)
BrownLow2<-projectRaster(BrownLow, crs = proj)
BrownHigh2<-projectRaster(BrownHigh, crs = proj)


plot(GreenLow2)
hist(GreenLow2)

plot(GreenHigh2)
hist(GreenHigh2)

plot(BrownLow2)
hist(BrownLow2)

plot(CompLow2)
hist(CompLow2)

plot(CompHigh2)
hist(CompHigh2)

plot(BrownLow2)
hist(BrownLow2)

plot(BrownHigh2)
hist(BrownHigh2)

differenceGreen<-GreenHigh2-GreenLow2
plot(differenceGreen, zlim=c(1000,6000))
hist(differenceGreen)

differenceBrown<-BrownHigh2-BrownLow2
plot(differenceBrown, zlim=c(1000,6000))
hist(differenceBrown)

differenceComp<-CompHigh2-CompLow2
plot(differenceComp, zlim=c(1000,9000))
hist(differenceComp)


writeRaster(GreenHigh2, "Input/Thresholds/GreenHigh.tif")
writeRaster(GreenLow2, "Input/Thresholds/GreenLow.tif")
writeRaster(BrownHigh2, "Input/Thresholds/BrownHigh.tif")
writeRaster(BrownLow2, "Input/Thresholds/BrownLow.tif")
writeRaster(CompHigh2, "Input/Thresholds/CompHigh.tif")
writeRaster(CompLow2, "Input/Thresholds/CompLow.tif")


